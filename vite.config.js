import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react-swc'

// https://vitejs.dev/config/
export default defineConfig({
  resolve: {
    alias:{
      "@routes": '/src/routes',
      "@components": '/src/components',
      "@types": '/src/types',
      "@mock": '/src/mock',
      "@hooks": '/src/hooks',
    }
  },
  plugins: [react()],
})
