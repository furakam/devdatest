/**
 * @typedef {import('../types/user.types').User}
 */


/**
 * @param {{user: User}} 
 * @returns 
 */
export default function UserCard({user}) {
  return (
    <div>
      <p className='text-red-700'>Username: <span>{user.username}</span></p>
    </div>
  )
}
