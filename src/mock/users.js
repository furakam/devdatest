/** 
 * @typedef {import('../types/user.types').User}
 */

import { faker } from "@faker-js/faker";


/**
 * 
 * @returns {User}
 */
export function createRandomUsers() {
  return {
    userId: faker.string.uuid(),
    username: faker.internet.userName(),
    email: faker.internet.email(),
    avatar: faker.image.avatarGitHub(),
    online: faker.datatype.boolean()
  } 
}


export const users = faker.helpers.multiple(createRandomUsers, {
  count: 50,
})