import { Card, MenuHandler, MenuItem, MenuList } from "@material-tailwind/react"
import { Avatar } from "@material-tailwind/react"
import { Chip } from "@material-tailwind/react"
import { Typography } from "@material-tailwind/react"
import { users } from "@mock/users"
import { Fragment } from "react"
import usePaginationCustom from "./hooks/usePaginationCustom"
import { ButtonGroup } from "@material-tailwind/react"
import { IconButton } from "@material-tailwind/react"
import { Menu } from "@material-tailwind/react"
import { Button } from "@material-tailwind/react"
import { ChevronDoubleLeftIcon, ChevronDoubleRightIcon, ChevronLeftIcon, ChevronRightIcon } from "@heroicons/react/24/outline"

export default function Pagination() {
  const { pagination, limitedData, limit, setLimit } = usePaginationCustom(users)

  const TABLE_HEAD = ["Id", "Avatar", "Username", "Email", "Status"]

  return (
    <Fragment>
      <div className="mb-5">
        <Typography variant="h1" className="text-blue-gray-900">Pagination example</Typography>
        <Typography variant="h4" className="text-teal-400 font-thin">Listado de usuarios fake</Typography>
      </div>
      <Card className="overflow-scroll h-full w-full">
        <table className="w-full min-w-max table-auto text-left">
          <thead>
            <tr className="text-center">
              {TABLE_HEAD.map((head) => (
                <th key={head} className="border-b border-blue-gray-100 bg-blue-gray-50 p-4">
                  <Typography
                    variant="small"
                    color="blue-gray"
                    className="font-normal leading-none opacity-70"
                  >
                    {head}
                  </Typography>
                </th>
              ))}
            </tr>
          </thead>
          <tbody>
            {limitedData && limitedData.map(({ userId, username, email, avatar, online }) => (
              <tr key={userId} className="even:bg-blue-gray-50/50 text-center">
                <td className="p-3">
                  <Typography variant="small" color="blue-gray" className="font-normal">
                    {userId}
                  </Typography>
                </td>
                <td className="p-3">
                  <Typography variant="small" color="blue-gray" className="font-normal">
                    <Avatar src={avatar} alt={username + '.png'} variant="rounded" size="sm" />
                  </Typography>
                </td>
                <td className="p-3">
                  <Typography variant="small" color="blue-gray" className="font-normal">
                    {username}
                  </Typography>
                </td>
                <td className="p-3">
                  <Typography variant="small" color="blue-gray" className="font-normal">
                    {email}
                  </Typography>
                </td>
                <td className="p-3">
                  <div className="flex justify-center">
                    {
                      online ?
                        <Chip
                          variant="ghost"
                          color="green"
                          size="sm"
                          value="Online"
                          icon={<span className="content-[''] block w-2 h-2 rounded-full mx-auto mt-1 bg-green-900" />}
                        /> :
                        <Chip
                          variant="ghost"
                          color="red"
                          size="sm"
                          value="Offline"
                          icon={<span className="content-[''] block w-2 h-2 rounded-full mx-auto mt-1 bg-red-900" />}
                        />
                    }
                  </div>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </Card>
      <div className="w-full flex flex-col gap-3 items-center my-3">
        <ButtonGroup variant="outlined" color="blue-gray">
          <IconButton onClick={pagination.first}>
            <ChevronDoubleLeftIcon strokeWidth={2} className="h-4 w-4" />
          </IconButton>
          <IconButton onClick={pagination.previous}>
            <ChevronLeftIcon strokeWidth={2} className="h-4 w-4" />
          </IconButton>
          {
            pagination.range.map((r, i) => (
              <IconButton className={pagination.active === r ? "bg-blue-gray-100 text-blue-gray-900" : ""} key={i} onClick={() => typeof (r) === "number" && pagination.setPage(r)}>{r == 'dots' ? '...' : r}</IconButton>
            ))
          }
          <IconButton onClick={pagination.next}>
            <ChevronRightIcon strokeWidth={2} className="h-4 w-4" />
          </IconButton>
          <IconButton onClick={pagination.last}>
            <ChevronDoubleRightIcon strokeWidth={2} className="h-4 w-4" />
          </IconButton>
        </ButtonGroup>
          <Menu
            animate={{
              mount: { y: 0 },
              unmount: { y: 25 },
            }}
          >
            <MenuHandler>
              <Button className="w-40">{limit} registros</Button>
            </MenuHandler>
            <MenuList>
              <MenuItem onClick={() => setLimit(5)}>5 registros</MenuItem>
              <MenuItem onClick={() => setLimit(10)}>10 registros</MenuItem>
              <MenuItem onClick={() => setLimit(20)}>20 registros</MenuItem>
            </MenuList>
          </Menu>
      </div>
    </Fragment>
  )
}
