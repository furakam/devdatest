/* eslint-disable react-hooks/exhaustive-deps */
import { usePagination } from "@mantine/hooks";
import { useEffect } from "react";
import { useState } from "react";

/**
 * 
 * @param {any[]} data 
 */
export default function usePaginationCustom(data) {
  const [limit, setLimit] = useState(5)
  const [page, setPage] = useState(1)
  const pagination = usePagination({ total: Math.ceil(data.length / limit) }, page, setPage)

  /**@type {[any[] | null, () => void]} */
  const [limitedData, setLimitedData] = useState(null)

  const getData = () => {
    const rangeInit = (pagination.active * limit) - limit
    const rangeLast = (pagination.active * limit)
    if (rangeInit > data.length){
      pagination.previous()
    }else {
      setLimitedData(data.slice(rangeInit, rangeLast))
    }
  }

  useEffect(() => {
    getData()
  }, [pagination.active, limit])

  return {
    pagination,
    limit,
    setLimit,
    limitedData,
    getData
  }
}
