import Navbar from "@components/Navbar/Navbar";
import { Outlet } from "react-router-dom";

export default function Root() {
  return (
    <>
      <Navbar />
      <main className="container mx-auto my-10">
        <Outlet />
      </main>
    </>
  )
}
