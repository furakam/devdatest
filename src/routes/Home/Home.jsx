import { Typography } from "@material-tailwind/react";
import { Fragment } from "react";

export default function Home() {
  return (
    <Fragment>
      <Typography variant="h1" className="font-light text-5xl">Hello world!</Typography>
    </Fragment>
  )
}
