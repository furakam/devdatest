import React from 'react'
import ReactDOM from 'react-dom/client'
import './index.css'

/* React router dom */
import { createBrowserRouter, RouterProvider } from "react-router-dom";
import Root from '@routes/Root';
import { ThemeProvider } from '@material-tailwind/react';
import Home from '@routes/Home/Home';
import Pagination from '@routes/Pagination/Pagination';


const router = createBrowserRouter([
  {
    path: '/',
    element: <Root />,
    children: [
      {
        path: '/',
        element: <Home />
      },
      {
        path: '/pagination',
        element: <Pagination/>
      }
    ]
  }
])


ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <ThemeProvider>
      <RouterProvider router={router} />
    </ThemeProvider>
  </React.StrictMode>,
)
